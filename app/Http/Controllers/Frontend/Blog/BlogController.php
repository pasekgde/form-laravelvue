<?php

namespace App\Http\Controllers\Frontend\Blog;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Blog\BlogRepository;
use Illuminate\Support\Facades\DB;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
/**
 * Class HomeController.
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

      protected $blogentry;

    public function __construct(BlogRepository $blogentry)
    {
        //Instance repository UserRepository kedalam property $user
        $this->blogentry = $blogentry;
    }


    public function all()
    {
    	return $this->blogentry->showall();
        // return view('frontend.blogs', ['blogsdata'=>$blogsdata] );
    }   

    public function index()
    {
        //return view('frontend.blogs');
        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);
        $blogsdata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(5);
        return view('frontend.blogs', ['blogsdata'=>$blogsdata],['alldata'=>$alldata] );
    }

    public function seoblog($slug)
    {
        $datablog = DB::table('blog_entries')->where('slug',$slug)->first();
        SEOMeta::setTitle($datablog->title);
        SEOMeta::setDescription($datablog->description);
        //SEOMeta::addMeta('article:created_at', $datablog->created_at->toW3CString(), 'property');
        //SEOMeta::addMeta('article:section', $post->category, 'property');
        SEOMeta::addKeyword(['sunat balita', 'sunat dewasa', 'sunat anak', 'sunat khusus', 'rumah sunat bali', 'sunat bali', 'sunatan','khitan']);

        OpenGraph::setDescription($datablog->description);
        OpenGraph::setTitle($datablog->page_title);
        //OpenGraph::setUrl('/blog/{$datablog->id}');
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        //OpenGraph::addImage($datablog->image->url);
        // OpenGraph::addImage($datablog->image->list('url'));
        //OpenGraph::addImage(['url' => 'http://image.url.com/cover.jpg', 'size' => 300]);
        //OpenGraph::addImage('http://image.url.com/cover.jpg', ['height' => 300, 'width' => 300]);
        
        JsonLd::setTitle($datablog->title);
        JsonLd::setDescription($datablog->description);
        JsonLd::setType('Article');
        //JsonLd::addImage($datablog->image->list('url'));
    }

    public function article($slug)
    {

        $this->seoblog($slug);

    	$blogsdata = $this->blogentry->find($slug);   
        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);

        return view('frontend.article', ['blogsdata'=>$blogsdata], ['alldata'=>$alldata] );
        
    }
}
