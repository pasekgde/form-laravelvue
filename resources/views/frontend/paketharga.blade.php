@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>Paket Harga</h1>
                <ul class="bread-crumb">
                    <li><a href="index.html">Home</a></li>
                    <li>Paket Harga</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

<!--Welcome Section-->
    <section class="welcome-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<br>
                 <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images\icons\sec-title-icon-1.png" alt=""></div>
            	<h2>Sirkumsisi / Sunat</h2>
                <div class="text">Semua paket harga tindakan sunat sudah termasuk konsultasi dokter, tidakan sunat, obat dan rawat luka dalam rentang penyembuhan normal. (All packages for circumcision procedures include doctor's consultation, circumcision treatment, medicine and wound care within normal healing ranges )
                </div>
                
                <div class="row clearfix">
                    <div class="content-side col-lg-6 col-md-6 col-sm-12 col-xs-12">

                      <ul class="accordion-box">
                            <li class="accordian-title">PAKET HARGA</li>
                            <!--Block-->
                            <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>• USIA BAYI & ANAK MAKSIMAL UMUR TAHUN</div>
                                <div class="acc-content current">
                                    <div class="content">
                                        <p>•   Usia bayi & anak maksimal umur 12 th : mulai 950.000  - 2.500.000<p>
                                        <p>•   Infant & maximum age 12 years: starting IDR 950,000 – IDR 2,500,000<p>
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>USIA REMAJA 13 - 17 TAHUN</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <p>•   Usia remaja 13 th -17 th : mulai 1.400.000 sampai 2.750.000</p>
                                        <p>•   Adolescence 13 years -17 years: from IDR 1,400,000 to IDR 2,750,000</p>
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-arrow-circle-right"></span> </div>USIA DEWASA</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <p>•    Usia dewasa : mulai 2.000.000 sampai 3.500.000</p>
                                        <p>•    Adult age: from IDR  2,000,000 to IDR  3,500,000</p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="sidebar-side col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <figure class="image">
                            <img src="{{ url('frontend/assets/')}}/images\paketharga.png" alt="">
                        </figure>
                    </div>

                </div>

            </div>
            
              <div class="sec-title centered">
                <BR>
                <div class="text">Detail biaya akan dijelaskan saat konsultasi sesuai dengan method yang digunakan . Perubahan harga dapat terjadi sewaktu – waktu dan dijelaskan oleh admin kami saat konsultasi. (Detailed costs will be explained during the consultation according to the method used. Price changes can occur at any time and are explained by our admin during consultation)
                </div>
            </div>
            
        </div>
    </section>
    <!--End Welcome Section-->

@endsection