@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')


    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Blog-->
                    <section class="blog-classic">
                        
                        
                        <!--News Style Four-->
                      @foreach ($blogsdata as $blog)
                        <div class="news-style-four">
                            <div class="inner-box">
                                <!--Image Column-->
                                <div class="image">
                                    <a href="{{ url('/blog/'.$blog->slug)}}"><img src="{{$blog->image}}" alt="" ></a>
                                </div>
                                <!--Content Column-->
                                <div class="content-column">
                                    <div class="inner">
                                        <div class="post-date">{{$blog->updated_at}}</div>
                                        <h3><a href="{{ url('/blog/'.$blog->slug)}}">{{$blog->title}}</a></h3>
                                        <ul class="post-meta">
                                            <li>by <span>{{$blog->author_name}}</span></li>
                                            <li><a href="blog-single.html"><span class="icon fa fa-commenting-o"></span> 7 Comments</a></li>    
                                            <li><a href="blog-single.html"><span class="icon fa fa-eye"></span>18 Views</a></li>
                                        </ul>
                                        <div class="text">{{$blog->summary}}</div>
                                       <a class="read-more" href="{{ url('/blog/'.$blog->slug)}}">Read More <span class="icon fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      @endforeach 
                        
                    </section>
					{{ $blogsdata->links() }}
                   
                   
                </div>
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        
                        <!--Services Post Widget-->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h3>Latest Post</h3>
                            </div>
                            <!--Post-->
                            @foreach($alldata as $latestpost)
                            <article class="post">
                                <figure class="post-thumb img-circle"><a href="{{url('blog/'.$latestpost->slug)}}"><img src="{{$latestpost->image}}" alt=""></a></figure>
                                <div class="text"><a href="{{url('blog/'.$latestpost->slug)}}">{{$latestpost->title}}</a></div>
                                <div class="post-info">Posted by {{$latestpost->author_name}}</div>
                            </article>
                            @endforeach
                            <!--Post-->
                        </div>
                        
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>>    

@endsection
