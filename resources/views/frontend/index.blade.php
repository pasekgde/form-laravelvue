@extends('frontend.layouts.app')
@section('content')
    <page1-component></page1-component>
    <page2-component></page2-component>
    <page3-component></page3-component>
    <page4-component></page4-component>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/homepage.js')) !!}
@stop