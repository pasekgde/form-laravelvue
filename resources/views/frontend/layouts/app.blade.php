<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NobleUI Responsive Bootstrap 4 Dashboard Template</title>
    <!-- core:css -->
    <link rel="stylesheet" href="{{ url('frontend//assets/')}}/vendors/core/core.css">
    <!-- endinject -->
  <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ url('frontend/assets/')}}/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
  <!-- Layout styles -->  
    <link rel="stylesheet" href="{{ url('frontend/assets/')}}/css/demo_5/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{ url('frontend/assets/')}}/images/favicon.png" />
</head>
<body>
    <div class="main-wrapper">
    <!-- partial:partials/_navbar.html -->
        <div class="horizontal-menu">
             @include('frontend.includes.topmenuinfo') 
             @include('frontend.includes.menu')
        </div>
    <!-- partial -->
        <div class="page-wrapper">
              
          <div id="app">
            <div class="page-content">
              <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="card">

                        <img src="{{url('frontend/assets/')}}/images/header.jpg" class="img-fluid" alt="Responsive image">

                    </div>
                  </div>
              </div>
              @yield('content')
            </div>            
          </div>
            <!-- partial:partials/_footer.html -->
            @include('frontend.includes.footer')
            <!-- partial -->
        </div>
    </div>

    <!-- core:js -->
    <script src="{{ url('frontend/assets/')}}/vendors/core/core.js"></script>
    <!-- endinject -->
  <!-- plugin js for this page -->
  <script src="{{ url('frontend/assets/')}}/vendors/chartjs/Chart.min.js"></script>
  <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.js"></script>
  <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.resize.js"></script>
  <script src="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="{{ url('frontend/assets/')}}/vendors/apexcharts/apexcharts.min.js"></script>
  <script src="{{ url('frontend/assets/')}}/vendors/progressbar.js/progressbar.min.js"></script>
    <!-- end plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ url('frontend/assets/')}}/vendors/feather-icons/feather.min.js"></script>
    <script src="{{ url('frontend/assets/')}}/js/template.js"></script>
    <!-- endinject -->
  <!-- custom js for this page -->
  <script src="{{ url('frontend/assets/')}}/js/dashboard.js"></script>
  <script src="{{ url('frontend/assets/')}}/js/datepicker.js"></script>
  <script src="{{ url('frontend/assets/')}}/js/file-upload.js"></script>
    <!-- end custom js for this page -->
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {{-- {!! script(mix('js/backend.js')) !!} --}}
    @stack('after-scripts')
    @yield('pagespecificscripts')
</body>
</html>    